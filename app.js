// const arr = ['travel', 'hello', 'eat', 'ski', 'lift']; 

// const result = arr.filter(item => item.length > 3).length; 

// console.log(result); 


let people = [
    {name: "Іван", 
    age: 25, 
    sex: "чоловіча" },
    { name: "Маша", 
    age: 30, 
    sex: "жіноча" },

    { name: "Максим", 
    age: 22, 
    sex: "чоловіча" },

    { name: "Олена",
    age: 27, 
    sex: "жіноча" }
];

let result = people.filter(man => man.sex === 'чоловіча');

console.log(result)
